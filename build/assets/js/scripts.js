var counterData = {counterStart: "2,025,629,824.00", initialDate: "10/25/2018 10:00", step: "11.57", counterInterval: "1"};
$(function () {

    if ($("#billingCounter span").length > 0) {

        var currentstat =  parseFloat(counterData['counterStart'].replace(/[^\d.-]/g,''));
        var intervalSeconds = counterData['counterInterval'] * 1000;
        var increaseUnit = counterData['step'];
        var initialDate = new Date(counterData["initialDate"]);

        setInterval(function () {

            var currentDate = new Date();
            var timeDiffSeconds = (currentDate - initialDate)/1000;
            var increment = timeDiffSeconds * increaseUnit;
            var currentstat2 = parseInt((currentstat + increment) * 100);
            currentstat2 = (currentstat2 / 100).toFixed(2);
            var num = String(currentstat2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            $("#billingCounter span").text(num);
        }, intervalSeconds);
    }
})