'use strict';

var gulp        = require('gulp'),
watch           = require('gulp-watch'),
autoprefixer    = require('gulp-autoprefixer'),
uglify          = require('gulp-uglify'),
sass            = require('gulp-sass'),
sourcemaps      = require('gulp-sourcemaps'),
rigger          = require('gulp-rigger'),
cssmin          = require('gulp-minify-css'),
imagemin        = require('gulp-imagemin'),
pngquant        = require('imagemin-pngquant'),
rimraf          = require('rimraf'),
browserSync     = require("browser-sync"),
reload          = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/assets/js/',
        css: 'build/assets/css/',
        img: 'build/assets/img/',
        fonts: 'build/assets/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/**/*.js',
        style: 'src/css/*.scss',
        csslibs: 'src/css/*.css',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/css/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};
var config = {
    server: {
        baseDir: "./build"
    },
    https: false,
    tunnel: false,
    open: false,
    reloadDelay: 1000,
    host: 'new.dentalbilling.com',
    port: 3000,
    logPrefix: "crizise",
};
gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
    });
gulp.task('js:build', function () {
    gulp.src(path.src.js)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});
gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(
            autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Safari >= 5.1.7', 'IE >= 10']})
        )
        // .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
    });
gulp.task('style:csslibs', function () {
    gulp.src(path.src.csslibs)
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
    });

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        // .pipe(imagemin({
        //     progressive: true,
        //     svgoPlugins: [{removeViewBox: false}],
        //     use: [pngquant()],
        //     interlaced: true
        // }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
    });
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});
gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'style:csslibs',
    'fonts:build',
    'image:build'
    ]);

gulp.task('webserver', function () {
    browserSync(config);
});
gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        setTimeout(function () {
            gulp.start('html:build');
        }, 1500);
    });
    watch([path.watch.style], function(event, cb) {
        setTimeout(function () {
            gulp.start('style:build');
        }, 1500);
    });
    watch([path.watch.js], function(event, cb) {

        setTimeout(function () {
            gulp.start('js:build');
        }, 1500);
    });
    watch([path.watch.img], function(event, cb) {
        setTimeout(function () {
            gulp.start('image:build');
        }, 1500);
    });
    watch([path.watch.fonts], function(event, cb) {
        setTimeout(function () {
            gulp.start('fonts:build');
        }, 1500);
    });
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);
